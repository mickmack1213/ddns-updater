import argparse
import ipaddress
import logging
import math
import os.path
import re
import time
from dataclasses import dataclass
from datetime import datetime
from ipaddress import IPv4Address, IPv6Address
from typing import List, Tuple, Dict, Optional

import requests
import yaml

ip4_finder = re.compile(
    "((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\.){3}(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])")
ip6_finder = re.compile(
    "(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))")

ip4_sources: List[str] = [
    "https://api.ipify.org/",
    "https://ip4.ddnss.de/meineip.php",
]
ip6_sources: List[str] = [
    "https://api6.ipify.org/",
    "https://ip6.ddnss.de/meineip.php",
]

offline_responses = {
    "https://ip4.ddnss.de/meineip.php": "0::0",
    "https://api.ipify.org/": "0.0.0.0",
    "https://ip6.ddnss.de/meineip.php": "0::0",
    "https://api6.ipify.org/": "0::0",
}


@dataclass(frozen=True)
class UpdateURLConfig:
    name: str
    url: str
    use4: bool
    use6: bool
    response_matcher: Optional[str]
    hosts: Optional[List[str]]
    custom: Dict[str, str]

    def __post_init__(self):
        if "ip4" in self.url and not self.use4:
            logging.warning("Update URL of {} contains ip4 although it does not use IPv4".format(self.name))
        if "ip6" in self.url and not self.use6:
            logging.warning("Update URL of {} contains ip6 although it does not use IPv6".format(self.name))


def _get_configs(config_file: str) -> Tuple[float, float, List[UpdateURLConfig]]:
    with open(config_file, "r") as file:
        data = yaml.safe_load(file)
        file.close()
    if data is None:
        logging.error("Config file {} is empty".format(config_file))
        exit(1)

    loop_time = None
    force_time = None
    configs = []

    name = "General"
    try:
        assert isinstance(data["loop-time"], float) or isinstance(data["loop-time"], int)
        assert isinstance(data["force-time"], float) or isinstance(data["force-time"], int)
        loop_time = data["loop-time"]
        force_time = data["force-time"]
        for name, config_input in data.items():
            if name == "loop-time":
                continue
            if name == "force-time":
                continue

            assert isinstance(config_input, dict)
            assert isinstance(config_input["url"], str)
            assert isinstance(config_input["use4"], bool)
            assert isinstance(config_input["use6"], bool)
            assert config_input["use4"] or config_input["use6"]

            response_matcher = None
            if "response-matcher" in config_input.keys():
                assert isinstance(config_input["response-matcher"], str)
                response_matcher = config_input["response-matcher"]

            hosts = None
            if "hosts" in config_input.keys():
                assert isinstance(config_input["hosts"], list)
                for host in config_input["hosts"]:
                    assert isinstance(host, str)
                hosts = config_input["hosts"]

            custom = {}
            for custom_name, custom_value in config_input.items():
                if custom_name in ["url", "use4", "use6", "response-matcher", "hosts"]:
                    continue
                assert isinstance(custom_value, str)
                custom[custom_name] = custom_value

            configs.append(UpdateURLConfig(
                name=name,
                use4=config_input["use4"],
                use6=config_input["use6"],
                url=config_input["url"],
                response_matcher=response_matcher,
                hosts=hosts,
                custom=custom
            ))
    except AssertionError as e:
        logging.error("{} configuration has an invalid structure".format(name))
        exit(1)
    except KeyError as e:
        logging.error("{} misses a configuration parameter: {}".format(name, e))
        exit(1)
    except Exception as e:
        logging.critical("Unexpected error while parsing {} config in {}: {}".format(name, config_file, e))
        exit(1)

    return loop_time, force_time, configs


def _query_ips(use4: bool, use6: bool, dry_run: bool) -> Tuple[Optional[IPv4Address], Optional[IPv6Address], datetime]:
    def query_ip4() -> Optional[IPv4Address]:
        ip4s = []

        for source in ip4_sources:
            try:
                if dry_run:
                    ip4s.append(ipaddress.IPv4Address(ip4_finder.search(offline_responses[source]).group()))
                else:
                    ip4s.append(ipaddress.IPv4Address(ip4_finder.search(requests.get(source).text).group()))
                logging.debug("Found IPv4 address: {} from {}".format(ip4s[-1], source))
            except requests.exceptions.ConnectionError:
                logging.debug("Failed to connect to {} for querying IPv4 address".format(source))
            except AttributeError:
                logging.debug("Can't parse answer from source {}".format(source))
            except Exception as e:
                logging.warning("Unexpected Exception while querying {} for IPv4: {}".format(source, e))
        unique_ip4s = set(ip4s)

        if len(unique_ip4s) > 1:
            logging.warning("Got ambiguous results for IPv4 address: {}".format(unique_ip4s))
            return None
        elif len(unique_ip4s) < 1:
            logging.warning("Failed querying IPv4 address")
            return None

        ip4 = unique_ip4s.pop()

        logging.debug("Queried IPv4 address {} ({} hits)".format(str(ip4), len(ip4s)))

        return ip4

    def query_ip6() -> Optional[IPv6Address]:
        ip6s = []
        for source in ip6_sources:
            try:
                if dry_run:
                    ip6s.append(IPv6Address(ip6_finder.search(offline_responses[source]).group()))
                else:
                    ip6s.append(IPv6Address(ip6_finder.search(requests.get(source).text).group()))
                logging.debug("Found IPv6 address: {} from {}".format(ip6s[-1], source))
            except requests.exceptions.ConnectionError:
                logging.debug("Failed to connect to {} for querying IPv6 address".format(source))
            except AttributeError:
                logging.debug("Can't parse answer from source {}".format(source))
            except Exception as e:
                logging.warning("Unexpected Exception while querying {} for IPv6: {}".format(source, e))

        unique_ip6s = set(ip6s)
        if len(unique_ip6s) > 1:
            logging.warning("Got ambiguous results for IPv6 address: {}".format(unique_ip6s))
            return None
        elif len(unique_ip6s) < 1:
            logging.warning("Failed querying IPv6 address")
            return None

        ip6 = unique_ip6s.pop()

        logging.debug("Queried IPv6 address {} ({} hits)".format(str(ip6), len(ip6s)))
        return ip6

    return query_ip4() if use4 else None, query_ip6() if use6 else None, datetime.now()


def _load_stored_ips(data_file: str) -> Tuple[datetime, Optional[IPv4Address], Optional[IPv6Address]]:
    data = None
    if os.path.exists(data_file):
        with open(data_file, "r") as file:
            data = yaml.safe_load(file)
            file.close()
    if data is None:
        data = {}

    last_time = datetime.fromtimestamp(0)
    ip4, ip6 = None, None

    for key, value in data.items():
        try:
            if key == "timestamp":
                last_time = datetime.fromisoformat(value) if isinstance(value, str) else value
            elif key == "ip4":
                ip4 = IPv4Address(value) if value != "None" else None
            elif key == "ip6":
                ip6 = IPv6Address(value) if value != "None" else None
        except Exception as e:
            logging.warning("Unexpected error while parsing {} from data file: {}".format(key, e))
    return last_time, ip4, ip6


def _select_ips(ip4: Optional[IPv6Address], ip6: Optional[IPv6Address], data_file: str, force_time: float,
                now: datetime) -> Tuple[bool, Optional[IPv4Address], Optional[IPv6Address]]:
    last_update_time, stored_ip4, stored_ip6 = _load_stored_ips(data_file)

    def log_update_reason(reason: str):
        logging.info(
            '{}, updating ddns: "{}" -> "{}" and "{}" -> "{}"'.format(reason, stored_ip4, ip4, stored_ip6, ip6))

    if (now - last_update_time).total_seconds() >= force_time:
        log_update_reason("Force time reached")
        need_to_update = True
    elif ip4 != stored_ip4 or ip6 != stored_ip6:
        log_update_reason("IPs changed")
        need_to_update = True
    else:
        logging.info("IPs didn't change, no update necessary")
        need_to_update = False

    return need_to_update, ip4, ip6


def _generate_replacements(ip4: Optional[IPv4Address], ip6: Optional[IPv6Address], config: UpdateURLConfig) \
        -> Dict[str, str]:
    replacements = {}
    ips = []
    if config.use4:
        ips.append(ip4)
        replacements["ip4"] = ip4
    if config.use6:
        ips.append(ip6)
        replacements["ip6"] = ip6
        replacements["ip6_first16"] = ip6.exploded[:5] if ip6 else "0:"
        replacements["ip6_first32"] = ip6.exploded[:10] if ip6 else "0:0:"
        replacements["ip6_first48"] = ip6.exploded[:15] if ip6 else "0:0:0:"
        replacements["ip6_first64"] = ip6.exploded[:20] if ip6 else "0:0:0:0:"
        replacements["ip6_first80"] = ip6.exploded[:25] if ip6 else "0:0:0:0:0:"
        replacements["ip6_first96"] = ip6.exploded[:30] if ip6 else "0:0:0:0:0:0:"
        replacements["ip6_first112"] = ip6.exploded[:35] if ip6 else "0:0:0:0:0:0:0:"
        replacements["ip6_first128"] = ip6.exploded if ip6 else "0000:" * 7 + "0000"
    replacements["ips"] = ",".join([str(ip) for ip in ips])

    if config.hosts:
        replacements["hosts"] = ",".join(config.hosts)
    for key, value in config.custom.items():
        if key in replacements.keys():
            logging.warning("Custom config parameter {} for {} overrides a queried parameter".format(key, config.name))
        replacements[key] = value

    for key, value in replacements.items():
        if value is None:
            replacements[key] = ""
        if not isinstance(value, str):
            replacements[key] = str(value)
    return replacements


def _store_ips(ip4: str, ip6: str, timestamp: datetime, data_file: str):
    with open(data_file, "w") as file:
        yaml.safe_dump({
            "timestamp": timestamp,
            "ip4": ip4,
            "ip6": ip6,
        }, file)
        file.close()
    logging.debug("Stored {} | {} in {} at {}".format(ip4, ip6, data_file, timestamp))


def ddns_update(config_file: str, data_file: str, verbosity: str, dry_run: bool):
    assert verbosity == "low" or verbosity == "high" or verbosity == "unsafe"
    logging.basicConfig(format="%(asctime)s %(levelname)s - %(msg)s",
                        level=logging.DEBUG if verbosity != "low" else logging.INFO)
    if not verbosity == "unsafe":
        logging.getLogger("urllib3").setLevel(1000)

    loop_time, force_time, configs = _get_configs(config_file)

    use4 = any([config.use4 for config in configs])
    use6 = any([config.use6 for config in configs])

    while True:
        loop_start_time = datetime.now()

        ip4, ip6, now = _query_ips(use4, use6, dry_run=dry_run)

        need_update, ip4, ip6 = _select_ips(ip4, ip6, data_file, force_time, now)
        any_success = False

        if dry_run:
            any_success = need_update
        elif need_update:
            for config in configs:
                if config.use4 and ip4 is None:
                    logging.warning("Can't update {} as querying of IPv4 failed".format(config.name))
                    continue
                if config.use6 and ip6 is None:
                    logging.warning("Can't update {} as querying of IPv6 failed".format(config.name))
                    continue

                replacements = _generate_replacements(ip4, ip6, config)
                update_url = config.url.format(**replacements)
                response = requests.get(update_url)
                logging.debug("{} - Server response: {}\n{}".format(config.name, response, response.text))
                if "badauth" in response.text:
                    logging.error("Authentication failed for {}".format(config.name))
                    exit(1)
                elif config.response_matcher is not None and re.search(config.response_matcher, response.text):
                    logging.info("Updated {} successfully".format(config.name))
                    any_success = True
                else:
                    logging.warning("Unexpected response for {}: {}\n{}"
                                    .format(config.name, response, response.text))
                    logging.warning("Will be treated as successful update")
                    any_success = True

        if any_success:
            _store_ips(str(ip4), str(ip6), now, data_file=data_file)

        if math.isclose(loop_time, 0):
            break
        else:
            loop_end_time = datetime.now()
            time.sleep(max(0.0, loop_time - (loop_end_time - loop_start_time).total_seconds()))


def _main():
    parser = argparse.ArgumentParser("ddns-updater", description="Update your dynamic DNS server with this script")
    parser.add_argument("config_file", type=str, help="Path to the config YAML file")
    parser.add_argument("--data-file", dest="data_file", type=str, required=False, default="data.yaml",
                        help="Path to the data file, where last queried IPs are stored")
    parser.add_argument("-v", "--verbose", action="store_true", help="Increase verbosity and print debug info")
    parser.add_argument("--unsafe-verbose", action="store_true",
                        help="Enable logging for imported modules, will leak credentials to logs")
    parser.add_argument("--dry-run", action="store_true",
                        help="Run the script but don't query the ips or send update request")
    args = parser.parse_args()

    if not os.path.exists(args.config_file):
        print("Configuration file \"{}\" does not exist".format(args.config_file))
        exit(1)
    if args.data_file is None:
        print("Unexpected error data_file is None")
        exit(1)
    if not os.path.exists(args.data_file):
        print("Data file \"{}\" does not exist".format(args.data_file))
        exit(1)

    verbosity = "low"
    if args.verbose:
        verbosity = "high"
        if args.unsafe_verbose:
            verbosity = "unsafe"

    ddns_update(args.config_file, args.data_file, verbosity, args.dry_run)


if __name__ == '__main__':
    _main()
