FROM python:3-alpine3.10
COPY ["./ddns-updater.py", "./requirements.txt", "./"]

RUN pip3 install -r requirements.txt

ENTRYPOINT ["python3", "ddns-updater.py"]
