# ddns-updater

Update basically any dynamic DNS server with this script. It provides a basic tool, that regularly queries the IPv4
and/or IPv6 address. If the addresses changed it will issue a GET request to update the DNS.

## Usage

### Standalone

```
usage: ddns-updater [-h] [--data-file DATA_FILE] [-v] [--unsafe-verbose] [--dry-run] config_file

Update your dynamic DNS server with this script

positional arguments:
  config_file           Path to the config YAML file

options:
  -h, --help            show this help message and exit
  --data-file DATA_FILE
                        Path to the data file, where last queried IPs are stored
  -v, --verbose         Increase verbosity and print debug info
  --unsafe-verbose      Enable logging for imported modules, will leak credentials to logs
  --dry-run             Run the script but don't query the ips or send update request

```

### With docker

```
docker run mickmack1213/ddns-updater [-h] [--data-file DATA_FILE] [-v] [--unsafe-verbose] [--dry-run] config_file
```

## Configuration

The configuration is done via a config YAML file.

```yaml
force-time: 86400
loop-time: 0
DDNS-Service1:
  url: "YOUR_URL_WITH_{key}_WORDS"
  use4: True
  use6: True
  response-matcher: ".*Updated [0-9]+ hostname"
  key: "CUSTOM KEY STRING -> WIll replace {key}"
  hosts:
    - "host1"
    - "host2"
DDNS-Service2:
  url: "YOUR_URL_WITH_{token}_WORDS"
  use4: True
  use6: True
  response-matcher: ".*Updated [0-9]+ hostname"
  token: "CUSTOM KEY STRING -> WIll replace {key}"
  hosts:
    - "host1"
    - "host2"
```

## Modify the Update URL

The update URL must be specified via the command line parameter and can make use of multiple keywords.

### Keyword List

| Keyword          | Meaning                                                                                                                                                                         |
|------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `{ip4} `         | The IPv4 queried address: `192.1.2.3`                                                                                                                                           |
| `{ip6} `         | The IPv6 queried address: `2001::1:2:3:4`                                                                                                                                       |
| `{ip6_first16}`  | The first 16 bits of the queried ip6. <br/> If ip6 not present: `0:`                                                                                                            |
| `{ip6_first32}`  | The first 32 bits of the queried ip6. <br/> If ip6 not present: `0:0:`                                                                                                          |
| `{ip6_first48}`  | The first 48 bits of the queried ip6. <br/> If ip6 not present: `0:0:0:`                                                                                                        |
| `{ip6_first64}`  | The first 64 bits of the queried ip6. <br/> If ip6 not present: `0:0:0:0:`                                                                                                      |
| `{ip6_first80}`  | The first 80 bits of the queried ip6. <br/> If ip6 not present: `0:0:0:0:0:`                                                                                                    |
| `{ip6_first96}`  | The first 96 bits of the queried ip6. <br/> If ip6 not present: `0:0:0:0:0:0:`                                                                                                  |
| `{ip6_first112}` | The first 112 bits of the queried ip6. <br/> If ip6 not present: `0:0:0:0:0:0:0:`                                                                                               |
| `{ip6_first128}` | Every bit of the queried ip6. This replacement has always a fixed length of 39, as no shortening is used. <br/> If ip6 not present: `'0000:0000:0000:0000:0000:0000:0000:0000'` |
| `{ips} `         | All activated and queried ips in a comma seperated list: `192.1.2.3,2001::1:2:3:4`                                                                                              |                                                                                                                   |
| `{hosts}`        | All hosts in a comma seperated list: `host1,host2,host3`                                                                                                                        |
| `{custom}`       | The custom entries in the YAML file                                                                                                                                             |

### Update URL Examples

| DNS Provider | Update URL                                                                 | status   |
|--------------|----------------------------------------------------------------------------|----------|
| noip.com     | `https://USER:PWD@dynupdate.no-ip.com/nic/update?hostname=HOST&myip={ips}` | untested |
| dyndns.org   | `https://USER:PWD@members.dyndns.org/v3/update?hostname=HOST&myip={ip4}`   | untested |
| ddnss.de     | `https://ddnss.de/upd.php?key=KEY&host=HOST&ip={ip4}&ip6={ip6}`            | tested   |

### Updating IPv6 for different device in local network

Assume you have a local device which can't update it's IPv& DDNS entries properly (e.g. NAS with locked firmware), and
which shall still be usable via IPv6. We also assume, that the link-local address is known and constant,
e.g. `fe80::1:2:3:4`.

That's the use case for the `ip6_first` replacements. You can select the correct prefix length from the requirements and
hardcode your IPv6 suffix into the update-url: `ip6={ip6_first64}1:2:3:4`, this example will use the global 64-bit
prefix of the device the script is running on and append the local IP6.

For non provided prefix length (multiples of 16) you can build it by yourself. It's sadly pretty lengthy and rather
tedious. Example for a different prefix lengths:

| Prefix-Length | Update URL                                            |
|---------------|-------------------------------------------------------|
| 4             | `{ip6_first128[0]}`                                   |
| 8             | `{ip6_first128[0]}{ip6_first128[1]}`                  |
| 12            | `{ip6_first128[0]}{ip6_first128[1]}{ip6_first128[2]}` |
| 20            | `{ip6_first16}{ip6_first128[5]}`                      |
| 36            | `{ip6_first16}{ip6_first128[10]}`                     |
| 52            | `{ip6_first16}{ip6_first128[15]}`                     |
| 68            | `{ip6_first16}{ip6_first128[20]}`                     |
| 84            | `{ip6_first16}{ip6_first128[25]}`                     |
| 100           | `{ip6_first16}{ip6_first128[30]}`                     |
| 116           | `{ip6_first16}{ip6_first128[35]}`                     |

## Passing credentials

The recommended way to pass the credentials is via a credentials file (example file below) and specify the path to the
file on the command line. This avoids that the credentials are stored in some kind of shell history etc.

Another discouraged way of passing the credentials to the script are the command line options.

Example file: `credentials.yaml`

```yaml
user: <username>
pwd: <password>
key: <key>
```

## Main Loop

How fast this script loops and how often it updates the dynamic DNS entry depends on two parameters. The loop-time and
force-time. The loop-time specifies the time between querying the IP-addresses. If the command line parameter specifying
this time is not present, the script will run an DNS update once and exit immediately.

After each query of the IPs they will be compared to the local stored IPs in the `data.yaml` file. There are two reasons
an update is issued:

- The IPs changed
- The time since the last update is greater than the force-time
